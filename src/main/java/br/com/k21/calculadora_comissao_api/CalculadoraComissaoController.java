package br.com.k21.calculadora_comissao_api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculadoraComissaoController {

    @RequestMapping("/calculadoraComissao")
    public double calcular(@RequestParam(value="valorVenda", defaultValue="0") double valorVenda) {
        return CalculadoraComissao.calcular(valorVenda);
    }
}