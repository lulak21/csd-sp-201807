package br.com.k21.calculadora_comissao_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculadoraComissaoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculadoraComissaoApiApplication.class, args);
	}
}
