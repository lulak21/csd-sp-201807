FROM openjdk:8-jdk-alpine

COPY target/calculadora_comissao_api-0.0.1.jar .

ENTRYPOINT ["java", "-jar", "./calculadora_comissao_api-0.0.1.jar"]